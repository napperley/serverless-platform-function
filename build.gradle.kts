group = "org.example"
version = "0.1-SNAPSHOT"

plugins {
    kotlin("multiplatform") version "1.4.10"
}

kotlin {
    linuxX64 {
        binaries {
            executable("knative_func") {
                entryPoint = "org.example.serverlessPlatform.func.main"
            }
        }
    }
}