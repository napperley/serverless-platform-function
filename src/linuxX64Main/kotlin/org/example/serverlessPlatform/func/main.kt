@file:Suppress("EXPERIMENTAL_UNSIGNED_LITERALS", "EXPERIMENTAL_API_USAGE")

package org.example.serverlessPlatform.func

import kotlinx.cinterop.CPointer
import platform.posix.*
import kotlinx.cinterop.toKString
import kotlinx.cinterop.memScoped
import kotlinx.cinterop.cstr
import kotlin.system.exitProcess

private val homeDir = getenv("HOME")?.toKString() ?: ""
private val filePath = "$homeDir/.serverless_platform/test.txt"

fun main() {
    println("Opening text file...")
    val file = openFile()
    println("Writing text file...")
    writeToFile(file)
    fclose(file)
    Unit
}

private fun printError(txt: String) {
    fprintf(stderr, txt)
}

private fun writeToFile(file: CPointer<FILE>?) = memScoped {
    val txt = "Hello from Kotlin Native serverless function! :)"
    val bytesWritten = fwrite(__s = file, __n = 1u, __size = txt.length.toULong(), __ptr = txt.cstr.ptr)
    if (bytesWritten < 1uL) {
        printError("Cannot write to file.")
        exitProcess(-1)
    }
}

private fun openFile(): CPointer<FILE>? {
    val writeMode = "w"
    val result = fopen(filePath, writeMode)
    if (result == null) {
        printError("Cannot open file ($filePath).")
        exitProcess(-1)
    }
    return result
}
